import java.util.*; //Imports utilities
public class RobotCity { //Beginning of class
public static void main(String[] args){ //beginning of main methof
int[][] cityarray = buildCity(); //Creates a new array by calling build city method
System.out.println(); //Prints line
System.out.println("City Built:"); //Prints City built
int width = cityarray.length; //Gets width
int height = cityarray[0].length; //gets length
display(cityarray); //Calls display method to show city
Random random = new Random();  //Random number generator
int k = random.nextInt((100 - 1) + 1) + 1; //generates random between 20 and 50
System.out.println(""+k+" robots invading"); //Prints amount of invading robots
System.out.println(); //Prints line
cityarray = invade(cityarray, k); //Calls invade method
System.out.println("Invaded:"); //Prints invaded
display(cityarray); //Calls display method to show city
for (int i = 1; i < 6; i++){ //runs 5 times
 cityarray = update(cityarray);  //Calls update method and sets to cityarray
 System.out.println("Update "+i+":"); //Prints what iteration of update city is on
 display(cityarray); //Calls dispay method to show city
}
} //End of main method
public static int[][] buildCity(){ //Start of buildCity method
Random random = new Random();  //Random number generator
int width = random.nextInt((15 - 10) + 1) + 10; //generates random number between 10 and 15
int height = random.nextInt((15 - 10) + 1) + 10; //generates random number between 10 and 15
int[][] cityarray = new int[width][height]; //Creates new array with random dimesions
for (int i=0; i < width; i++){ //runs for length of width
  for (int j=0; j < height; j++){ //runs for length of height
    cityarray[i][j] = random.nextInt((999 - 100) + 1) + 100; //generates number between 100 and 999 and sets to each memeber of 2d array
  }
}
return cityarray; //returns cityarray
} //End of buildCity method
public static void display(int[][] cityarray){ //Start of display method
int width = cityarray.length; //Finds width
int height = cityarray[0].length; //finds length
System.out.println(); //Prints line
for (int i=0; i < width; i++){ //runs for width
  for (int j=0; j < height; j++){ //runs for height
    if (cityarray[i][j] < 0){ //runs if negative, no space, taken by negative sign
    }
    else { //runs if positive
          System.out.print(" ");//Prints a space
    }
    System.out.print(cityarray[i][j]); //prints member of array
    System.out.print(" "); //prints Space
 }
   System.out.println(); //Prints line
   System.out.println(); //Prints line
}
}  //End of display method
public static int[][] invade(int[][] cityarray, int k){ //Creates method for invade
Random random = new Random();  //Random number generator
int width = cityarray.length; //Find width
int height = cityarray[0].length; //Finds length
int coordinate1 = 0; //Creates int for coordinate 1
int coordinate2 = 0; //Creates int for coordinate 2
int test = 0; //Creates a int for test
for (int i = k; i > 0; i--){ //Runs for length of k
  coordinate1 = random.nextInt(width); //Generates coordinate
  coordinate2 = random.nextInt(height); //Generates coordinate
  test = cityarray[coordinate1][coordinate2]; //copies to test
  if (test < 0){ //runs if negative
    i += 1; //adds to i, makes it generate another one, so duplicates
  }
  else { //Runs if positive
    cityarray[coordinate1][coordinate2] = cityarray[coordinate1][coordinate2] - (2*cityarray[coordinate1][coordinate2]); //Makes memebr of array negative
  }
}
return cityarray; //Returns cityarray
} //End of invade method
public static int[][] update(int[][] cityarray){ //Start of update method
int width = cityarray.length; //Gets width
int height = cityarray[0].length;  //Gets length
for (int i = 0; i < width; i++){ //runs for length of width
  for (int j = height-1; j > 0; j--){ //runs right to left excluding index 0
    if (cityarray[i][j-1] < 0 && cityarray[i][j] > 0){ //Checks that number is not already negative
      cityarray[i][j] = cityarray[i][j] - (2*cityarray[i][j]); //Makes number negative
      cityarray[i][j-1] = -1*cityarray[i][j-1];  //Makes left one positive
    }
  }
}
return cityarray; //Returns cityarray
} //End of update method
}//End of Class