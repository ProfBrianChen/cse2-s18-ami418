import java.util.*; //Imports utilities
public class CSE2Linear { //creates class
public static void main(String[] args){ //Start of main method
int[] grades = new int[15]; //creates and allocates space for 
String junkword = "a"; //Creates a junkword for scanner
boolean range = true; //creates boolean for range
boolean ascending = true; //creates boolean for testing ascending
boolean hasinteger = true; //boolean for if input has integer
Scanner myScanner = new Scanner(System.in); //Creates scanner
do{ //Start of do loop
hasinteger = true; //boolean for determineing strings and integers
System.out.print("Enter 15 ascending ints for final grades in CSE2: "); //Prompts user input for 15 grades
  for (int i = 0; i < 15; i++){ //Runs when i is less than 15, the length of the array
    if (myScanner.hasNextInt()){ //Runs if user enters an int
    grades[i] = myScanner.nextInt();  //Sets index i of grades to user input
    }   
  else { //runs if user enteres something other than an int
      junkword = myScanner.next(); //gets rid of string
      hasinteger = false; //sets hasinteger to false, making loop run again
    } 
  }
  if (!hasinteger){ //Runs if user doesn't enter an int
    System.out.println("Error, you entered a string."); //Tells user of error
  }
  range = range(grades); //calls method for checking that ints are within range
  ascending = ascending(grades); //calls method for checking that ints are ascending
} while (!range | !ascending | !hasinteger); //While part of loop runs first time. and then if any of the booleans are false
System.out.print("Your array is "); //Prints statement
System.out.println(listArray(grades)); //calls method to print array
int search1 = 0;  //creates int variable for first search
for (int i = 0; i == 0;){ //for loop for user input of first search
System.out.print("Enter a grade to search for: "); //Prompts user input for first search
  if (myScanner.hasNextInt()){ //runs if user inputs an int
    search1 = myScanner.nextInt(); //sets search value to user input
    i = 1; //stops loop
  }
  else { //runs if user doesn't enter an int
    System.out.println("Error, please enter an integer."); //Tells user of error
    junkword = myScanner.next(); //gets rid of user input
  }
  if (search1 > 100 || search1 < 0){ //runs if search is out of range
    System.out.println("Please enter a grade within the correct range."); //tells user of error
    search1 = 0; //sets search back to zero
    i = 0; //continues loop
  }
}
int amount1 = binarysearch(grades, search1); //calls method to find amount of times search occurs
if (amount1 > 0){ //Runs if numbers are found
	System.out.println(""+search1+" was found "+amount1+" times." ); //prints the amount that a search number appears
}
else { //runs if that number is not in the array
	System.out.println(""+search1+" was not found"); //tells user nothing was found
}
grades = Shuffler(grades); //calls method to shuffle grades
System.out.println("Shuffled"); //Tells user it has been shuffled
System.out.print("Your array is now: "); //Start of print statement of array
System.out.println(listArray(grades)); //calls method to print array
int search2 = 0;  //variable for second search
for (int i = 0; i == 0;){ //creates loop for second search
System.out.print("Enter a grade to search for: "); //prompts user input for grade to search of
  if (myScanner.hasNextInt()){ //runs if user inputs an integer
    search2 = myScanner.nextInt(); //sets search equal to user input
    i = 1; //stops loop
  }
  else { //runs if user doesn't enter an int
    System.out.println("Error, please enter an integer."); //Prints error message
    junkword = myScanner.next(); //throws away user input
  }
  if (search2 > 100 || search2 < 0){ //Runs if input is out of range
    System.out.println("Please enter a grade within the correct range."); //Prints error
    search2 = 0; //resets search
    i = 0; //continues loop
  }
}
int amount2 = linearsearch(grades, search2); //calls a method to get amout of times occured
if (amount2 > 0){ //runs if number occurs
	System.out.println(""+search2+" was found "+amount2+" times." ); //tells user how many times it was found
}
else { //Prints if number wasnt found
	System.out.println(""+search2+" was not found"); //Tells user nothing was found
}
} //end of main method
public static String listArray(int num[]){ //creates method for printing array
String out="{"; //creates string out
for(int j=0;j<num.length;j++){ //runs for the length of the array
  if(j>0){ //runs when not zero
   out+=", "; //adds to variable out
  }
out+=num[j]; //adds string to variable out
}
out+="} "; //adds to variale out
return out; //returns string variable out
} //end of listArray method
public static boolean range(int[] grades){ //method to test range
boolean range = true; //sets boolean variable for range default to true
int tester = 0; //creates and sets tester to 0
for (int i = 0; i < 15; i++){ //loop that runs when i is less than 15, the length of the array
  if (grades[i] < 0 || grades[i] > 100){ //runs if grades are out of ranfge
    range = false; //sets range to false
    tester = 1; //sets tester to 1
  }
}
if (tester == 1){ //runs if tester is set to 1
  System.out.println("Error, numbers are out of grade range."); //Prints error
}
return range; //returns boolean range
} //end of range method
public static boolean ascending(int[] grades){ //creates method to test order
boolean ascending = true; //sets boolean vaeriabel to true as default
int tester = 0; //creates and sets test to 0
for (int i = 0; i < 14; i++){ //runs when i is less than 14, to determine ascending
  if (grades[i] > grades[i+1]){ //runs if grades are out of order
    ascending = false; //sets ascending to false
		tester = 1; //sets tester to 1
  }
  if (grades[13] > grades[14]){ //runs if grades are out of order
    ascending = false; //sets ascening to false
    tester = 1; //sets tester to 1
  }
}
if (tester == 1){ //runs if tester is 1
  System.out.println("Error, numbers are not in ascending order."); //Prints error
}
return ascending; //returns boolean ascending
} //end of ascending method
public static int[] Shuffler(int[] grades){ //creates method for shuffling grades
Random random = new Random();  // Random number generator			
for (int i=0; i<grades.length; i++) { //runs when i is less than the length of the array grades
	int randomPosition = random.nextInt(grades.length); //generates a random position
	int holder = grades[i]; //sets holder to grades index i
	grades[i] = grades[randomPosition]; //sets grades idex i to the grades of index of random
	grades[randomPosition] = holder; //sets grades idex of the random position to the holder
} 
return grades; //returns array grades
} //end of shuffler method
public static int binarysearch(int[] grades, int search){ //Creates method to search
int amount = 0; //sets default amount to 0
for (int i = 0; i < grades.length; i++){ //runs when i is less than the length of array grades
	if (search == grades[i]){ //runs if search is equal to number in index i of grades
		amount += 1; //adds one to the amount
	}
}
return amount; //returns the int amount
} //end of binarysearch method
public static int linearsearch(int[] grades, int search){ //Creates method to search
int amount = 0; //sets default amount to 0
for (int i = 0; i < grades.length; i++){ //runs when i is less than the length of array grades
	if (search == grades[i]){ //runs if search is equal to number in index i of grades
		amount += 1; //adds one to the amount
	}
}
return amount; //returns the int amount
} //end of linearsearch method
} //end of class