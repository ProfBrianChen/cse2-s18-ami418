import java.util.Scanner; //imports scanner utility
import java.util.Random; //imports random utility
public class RemoveElements{ //creates class
public static void main(String [] arg){ // creates main method
Scanner scan=new Scanner(System.in); //Creates scanner
int num[]=new int[10]; //creates an array and allocates space
int newArray1[]; //creates new array
int newArray2[]; //cretaeds new array
int index,target; //Creates new array
	String answer=""; //Creates string variable for answer
	do{ //start of do while loop
  	System.out.print("Random input 10 ints [0-9] "); //prints that program generates random ints
  	num = randomInput(); //calls method to generate array
  	String out = "The original array is:"; //prints "the original array is: "
  	out += listArray(num); //adds list array to the output
  	System.out.println(out); //prints output
 
  	System.out.print("Enter the index "); //Prompts user to enter index
  	index = scan.nextInt(); //sets index equal to next int
  	newArray1 = delete(num,index); //calls delete method and sets equal to newArray1
  	String out1="The output array is: "; //prints "the output array is: "
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1); //Prints newArray1
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt(); //sets target equal to next int
  	newArray2 = remove(num,target); //calls remove method and serts equal to newArray2
  	String out2="The output array is "; //prints "the output array is: "
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2); //Prints newArray2
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit- "); //Prompts user if they wish to run program again
  	answer=scan.next(); //Sets asnwer to next string user enters
	}while(answer.equals("Y") || answer.equals("y")); //runs first time, and then if user has enteered "Y" or "y"
  } //end of main method
public static String listArray(int num[]){ //Creates new method to print array
	String out="{"; //Creates a string and stars the proper syntax
	for(int j=0;j<num.length;j++){ //loop that runs to print array
  	if(j>0){ //runs after 0
    	out+=", "; //adds a comma
  	}
  	out+=num[j]; //adds the integer
	}
	out+="} "; //adds the final curly bracket
	return out; //returns string out
  } //end of listArray method
public static int[] randomInput(){ //Creates a method for generating an array
int[] array = new int[10]; //creates an array and allocates space
Random randomGenerator = new Random(); //Creates random generator   
for (int i = 0; i < 10; i++){ //loop that runs when i is less than 10, the length of the array
  array[i] = randomGenerator.nextInt(10); //generates a random number 0-9
  }
return array; //returns array
} //end of randomInput method
public static int[] delete(int[] num, int index){ //creates method for deleting a number in an certain index
int[] copyarray = new int[(num.length)-1]; //runs for the length of num minus one
for (int i = 0; i < index; i++){ //runs while i is less than the index
  copyarray[i] = num[i]; //sets copyarray to the same as num array for each index below i
}
for (int i = index; i < copyarray.length; i++){ //runs once desired index is reached, when i is between hte desired index and the length
  copyarray[i] = num[i+1]; //sets copyarray to next index up above in the num array, removing the user inputed index
}
return copyarray; //returns copyarray
} //end of delete method
public static int[]  remove(int[] num, int target){
int[] array = new int[num.length]; //creates array of same length as num
array = copy(num, array); //calls copy method to copy num to array
int counter = 0; //creates and sets counter to 0;
for (int i = 0; i < array.length; i++){ //Loop that runs when i is less than array length
	if (array[i] == target){ //runs if number in array index i is equal to the target
		counter += 1; //adds to counter
	}	
}
int index = 0; //creates and sets int index to zero
if (counter > 0){ //runs when counter is greater than 0
	for (int i = 0; i < array.length; i++){ //Runs when i is less than the array length
		if (array[i] == target){ //runs when the array at index i is equal to the target
			index = i; //sets index to i
			array = delete(array, index); //deletes number at index i from array
		}
	}
}
else { //runs if counter is still 0
	System.out.println("No matches were found."); //Tells user no matches were found
}
return array; //returns array
}
public static int[] copy(int original[], int copy[]){ //Creates method to copy arrays
  for (int i = 0; i < 10; i++){ //runs when i is less than 10
    copy[i] = original[i]; //copies array index by index
  }
return copy; //returns copy
} //end of copy method
} //end of class