//ami418 Asher Illick-Frank hw02
// 
public class Arithmetic {
    	// main method required for every Java program
   	public static void main(String[] args) {
 //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantPrice = 34.98;
//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;
//Number of belts
int numBelts = 1;
//cost per belt
double beltPrice = 33.99;
//the tax rate
double paSalesTax = 0.06;
//total cost of pants
double totalCostOfPants=numPants*pantPrice;
//total cost of shirts
double totalCostOfShirts=numShirts*shirtPrice;
//total cost of belts
double totalCostofBelts=numBelts*beltPrice;
      //prints cost of pants
              System.out.println("The total cost of pants is $"+totalCostOfPants+".");
      //prints cost of shirts
              System.out.println("The total cost of shirts is $"+totalCostOfShirts+".");
      //prints cost of belts
              System.out.println("The total cost of belts is $"+totalCostofBelts+".");
//total sales tax for pants
double salesTaxPants=totalCostOfPants*paSalesTax;
//total sales tax for shirts
double salesTaxShirts=totalCostOfShirts*paSalesTax;
//total sales tax for belts
double salesTaxBelts=totalCostofBelts*paSalesTax; 
      //converts sales tax of pants into two decimal places
    double stp2=salesTaxPants*100;
    int stp3= (int) stp2;
    double finalSalesTaxPants= (double) stp3/100;
      //converts sales tax of shirts into two decimal places
    double sts2=salesTaxShirts*100;
    int sts3= (int) sts2;
    double finalSalesTaxShirts= (double) sts3/100;
      //converts sales tax of belts into two decimal places
    double stb2=salesTaxBelts*100;
    int stb3= (int) stb2;
    double finalSalesTaxBelts= (double) stb3/100;
      //prints sales tax of pants
              System.out.println("The sales tax for pants is $"+finalSalesTaxPants+".");
      //prints sales tax of shirts
              System.out.println("The sales tax for shirts is $"+finalSalesTaxShirts+".");
      //prints sales tax of belts
              System.out.println("The sales tax for belts is $"+finalSalesTaxBelts+".");
//total cost of purchase (before tax)
double purchaseTotal=totalCostOfPants+totalCostOfShirts+totalCostofBelts;
      //prints total cost of purchase before tax
              System.out.println("The total cost of the purchase before tax is $"+purchaseTotal+".");
//total sales tax
double taxTotal=salesTaxPants+salesTaxShirts+salesTaxBelts;
      //converts total sales tax into two decimal places
    double tt2=taxTotal*100;
    int tt3= (int) tt2;
    double finalTaxTotal= (double) tt3/100;
      //prints total sales tax
              System.out.println("the total sales tax is $"+finalTaxTotal+".");
//total purchase total including sales tax
double Total=purchaseTotal+taxTotal;
      //converts purchase total including sales tax into two decimals
    double t2=salesTaxShirts*100;
    int t3= (int) t2;
    double finalTotal= (double) t3/100;
      //prints purchase total including sales tax
              System.out.println("The total paid for this transaction, including sales tax is $"+finalTotal+".");
	}  //end of main method   
} //end of class