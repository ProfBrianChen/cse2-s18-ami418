//ami Asher Illick-Frank
//hw04 Yahtzee 2/20/18

import java.util.Scanner; //Imports Scanner utility
import java.util.Random; //Imports Random utility

public class Yahtzee { //Creates main class Yahtzee
  public static void main(String[] args) { //Main method required for every java program

Scanner myScanner; //named scanner
myScanner = new Scanner( System.in ); //created scanner
String randomtest = "random"; //creates variable for later use in testing user input
    System.out.print("Enter the desired roll in the form of xxxxx or type 'random': "); //Prompts user to input their roll or choose a random roll
String roll = myScanner.nextLine(); //Creates variable for user input
int die1 = 0; //Creates variable for first die
int die2 = 0; //Creates variable for second die
int die3 = 0; //Creates variable for third die
int die4 = 0; //Creates variable for fourth die
int die5 = 0; //Creates variable for fifth die
int rollnumber = 0; //Creates variable for user input
if ( roll.equals(randomtest)) { //tests if user inputed "random"
  System.out.println("You picked random!"); //Shows user program is running properly
  Random pick1 = new Random(); //generates first random number from 1 to 6
    die1 = pick1.nextInt(6) + 1;
   Random pick2 = new Random(); //generates second random number from 1 to 6
    die2 = pick2.nextInt(6) + 1;
   Random pick3 = new Random(); //generates third random number from 1 to 6
    die3 = pick3.nextInt(6) + 1; 
   Random pick4 = new Random(); //generates fourth random number from 1 to 6
    die4 = pick4.nextInt(6) + 1;
   Random pick5 = new Random(); //generateds fifth random number from 1 to 6
    die5 = pick5.nextInt(6) + 1;
}
  else if ( roll != randomtest ) { //Runs if user inputs data and not "random"
   System.out.println("You picked your own numbers!"); //Shows user program is aware that they entered numbers
rollnumber = Integer.parseInt(roll); //Turns string into integer
  die1 = (rollnumber/10000); //isolates first digit
  die2 = (rollnumber/1000)-(die1*10); //isolates second digit
  die3 = (rollnumber/100)-((die1*100)+(die2*10)); //isloates third digit
  die4 = (rollnumber/10)-((die1*1000)+(die2*100)+(die3*10)); //isolates fourth digit
  die5 = (rollnumber - ((rollnumber/10)*10)); //isolates fifth digit
}
if ( die1 > 6){ //Runs if die 1 is greater than 6
      System.out.println("Error, invalid roll"); //Prints error
      System.exit(0); //Exits program
}
if (die2 > 6){ //Runs is die 2 is greater than 6
        System.out.println("Error, invalid roll"); //Prints error
        System.exit(0); //Exits program
}
if (die3 > 6){ //Runs if die 3 is greater than 6
        System.out.println("Error, invalid roll"); //Prints error
        System.exit(0); //Exits program
}
if (die4 > 6){ //Runs if die 4 is greater than 6
        System.out.println("Error, invalid roll"); //Prints error
        System.exit(0); //Exits program
}
if (die5 > 6){ //Runs if die 5 is greater than 6
        System.out.println("Error, invalid roll"); //Prints error
        System.exit(0); //Exits program
} 
  System.out.println("You rolled "+die1+" "+die2+" "+die3+" "+die4+" "+die5+""); //Prints what five numbers on the dice
int aces = 0; //Creates variable for total of 1's
  int a1 = 0; //Creates variable used in calclulations for 1's on die 1
  int a2 = 0; //Creates variable used in calclulations for 1's on die 2
  int a3 = 0; //Creates variable used in calclulations for 1's on die 3
  int a4 = 0; //Creates variabale used in calclulations for 1's on die 4
  int a5 = 0; //Creates variable used in calclulations for 1's on die 5
int twos = 0; //Creates variable for total of 2's
  int b1 = 0; //Creates variable used in calclulations for 2's on die 1
  int b2 = 0; //Creates variable used in calclulations for 2's on die 2
  int b3 = 0; //Creates variable used in calclulations for 2's on die 3
  int b4 = 0; //Creates variable used in calclulations for 2's on die 4
  int b5 = 0; //Creates variable used in calclulations for 2's on die 5
int threes = 0; //Creates variable for total of 3's
  int c1 = 0; //Creates variable used in calculations for 3's on die 1
  int c2 = 0; //Creates variable used in calculations for 3's on die 2
  int c3 = 0; //Creates variable used in calculations for 3's on die 3
  int c4 = 0; //Creates variable used in calculations for 3's on die 4
  int c5 = 0; //Creates variable used in calculations for 3's on die 5
int fours = 0; //Creates variable for total of 4's
  int d1 = 0; //Creates variable used in calculations for 4's on die 1
  int d2 = 0; //Creates variable used in calculations for 4's on die 2
  int d3 = 0; //Creates variable used in calculations for 4's on die 3
  int d4 = 0; //Creates variable used in calculations for 4's on die 4
  int d5 = 0; //Creates variable used in calculations for 4's on die 5
int fives = 0; //Creates variable for total 5's
  int e1 = 0; //Creates variable used in calculations for 5's on die 1
  int e2 = 0; //Creates variable used in calculations for 5's on die 2
  int e3 = 0; //Creates variable used in calculations for 5's on die 3
  int e4 = 0; //Creates variable used in calculations for 5's on die 4
  int e5 = 0; //Creates variable used in calculations for 5's on die 5 
int sixes = 0; //Creates variable for total 6's
  int f1 = 0; //Creates variable used in calculations for 6's on die 1
  int f2 = 0; //Creates variable used in calculations for 6's on die 2
  int f3 = 0; //Creates variable used in calculations for 6's on die 3
  int f4 = 0; //Creates variable used in calculations for 6's on die 4
  int f5 = 0;  //Creates variable used in calculations for 6's on die 5   
switch( die1 ){ //Switch statment for determining the value of die 1 for later calculations (three of a kind, four of kind, full house, yahtzee)
   case 1:
     a1 = 1;
     break;
   case 2:
     b1 = 2;
     break;
   case 3:
     c1 = 3;
     break;
   case 4:
     d1 = 4;
     break;
   case 5:
     e1 = 5;
     break;
   case 6:
     f1 = 6;
     break;     
 }
switch( die2 ){ //Switch statment for determining the value of die 2 for later calculations (three of a kind, four of kind, full house, yahtzee)
   case 1:
     a2 = 1;
     break;
   case 2:
     b2 = 2;
     break;
   case 3:
     c2 = 3;
     break;
   case 4:
     d2 = 4;
     break;
   case 5:
     e2 = 5;
     break;
   case 6:
     f2 = 6;
     break; 
}    
switch( die3 ){ //Switch statment for determining the value of die 3 for later calculations (three of a kind, four of kind, full house, yahtzee)
   case 1:
     a3 = 1;
     break;
   case 2:
     b3 = 2;
     break;
   case 3:
     c3 = 3;
     break;
   case 4:
     d3 = 4;
     break;
   case 5:
     e3 = 5;
     break;
   case 6:
     f3 = 6;
     break; 
}    
switch( die3 ){ //Switch statment for determining the value of die 4 for later calculations (three of a kind, four of kind, full house, yahtzee)
   case 1:
     a3 = 1;
     break;
   case 2:
     b3 = 2;
     break;
   case 3:
     c3 = 3;
     break;
   case 4:
     d3 = 4;
     break;
   case 5:
     e3 = 5;
     break;
   case 6:
     f3 = 6;
     break; 
}
switch( die4 ){ //Switch statment for determining the value of die 4 for later calculations (three of a kind, four of kind, full house, yahtzee)
   case 1:
     a4 = 1;
     break;
   case 2:
     b4 = 2;
     break;
   case 3:
     c4 = 3;
     break;
   case 4:
     d4 = 4;
     break;
   case 5:
     e4 = 5;
     break;
   case 6:
     f4 = 6;
     break; 
}
switch( die5 ){ //Switch statment for determining the value of die 5 for later calculations (three of a kind, four of kind, full house, yahtzee)
   case 1:
     a5 = 1;
     break;
   case 2:
     b5 = 2;
     break;
   case 3:
     c5 = 3;
     break;
   case 4:
     d5 = 4;
     break;
   case 5:
     e5 = 5;
     break;
   case 6:
     f5 = 6;
     break; 
}
aces = a1 + a2 + a3 + a4 + a5; //Finds sum of all 1's
twos = b1 + b2 + b3 + b4 + b5; //Finds sum of all 2's
threes = c1 + c2 + c3 + c4 + c5; //Finds sum of all 3's
fours = d1 + d2 + d3 + d4 + d5; //Finds sum of all 4's
fives = e1 + e2 + e3 + e4 + e5; //Finds sum of all 5's
sixes = f1 + f2 + f3 + f4 + f5; //Finds Sum of all 6's 
int tok = 0; //Creates variable for three of a kind
int fok = 0; //Creates variable for four of a king
int fh = 0; //Creates a variable for full house (actually the value of the pair in the full house, which is later added to the three of a kind)
int smst = 0; //Creates a variable for small straight
int lgst = 0; //Creates a variable for large straight
int ytz = 0; //Creates a variable for yahtzee (5 of a kind)
int chnc = 0; //Creates a variable for change
if (aces == 5){ //Runs if there are 5 1's
    ytz = 5; //Makes yahtzee equal to 5
}
if (twos == 10){ //Runs if there are 5 2's
    ytz = 10; //Makes yahtzee equal to 10
}
if (threes == 15){ //Runs if there are 5 3's
    ytz = 15; //Makes yahtzee equal to 15
}
if (fours == 20){ //Runs if there are 5 4's
    ytz = 20; //makes yahtzee equal to 20
}
if (fives == 25){ //Runs if there are 5 5's
    ytz = 25; //makes yahtzee equal to 25
}
if (sixes == 20){ //Runs if there are 5 6's
    ytz = 30; //makes yahzee equal to 30
}  
if (aces == 4){ //Runs if there are 4 1's
    fok = 4; //makes four of a kind equal to 4
}
if (twos == 8){ //Runs if there are 4 2's
    fok = 8; //makes four of a kind equal to 8
}
if (threes == 12){ //Runs if there are 4 3's
    fok = 12; //makes four of a kind equal to 12
}
if (fours == 16){ //Runs if there are 4 4's
    fok = 16; //makes four of a kind equal to 16
}
if (fives == 20){ //Runs if there are 4 5's
    fok = 20; //makes four of a kind equal to 20
}
if (sixes == 24){ //Runs if there are 4 6's
    fok = 24; //makes four of a kind equal to 24
}
if (aces == 3){ //Runs if there are 3 1's
    tok = 3; //makes three of a kind equal to 3
}
if (twos == 6){ //Runs if there are 3 2's
    tok = 6; //makes three of a kind equal to 6
}
if (threes == 9){ //Runs if there are 3 3's
    tok = 9; //makes three of a kind equal to 9
}
if (fours == 12){ //Runs if there are 3 4's
    tok = 12; //makes three of a kind equal to 12
}
if (fives == 15){ //Runs if there are 3 5's
    tok = 15; //makes three of a kind equal to 15
}
if (sixes == 18){ //Runs if there are 3 6's
    tok = 18; //makes three of a kind equal to 18
}
if (ytz > 0){ //Runs if Yahtzee is greater than 0
  System.out.println("You got Yahtzee!"); //Prints "You got Yahtzee!"
}
if (fok > 0){ //Runs if four of a kind has a value
  System.out.println("You got four of a kind!"); //Prints "You got four of kind!"
}
if (tok > 0){ //Runs if three of a kind
  System.out.println("You got three of a kind!"); //Prints "You got three of a kind"
}
if ( aces >= 1){ //Checks for large straight from 1-5, runs if roll is 1 2 3 4 5 in any order
  if (twos >= 2){
    if (threes >= 3){
      if (fours >= 4){
        if (fives >= 5){
          lgst = 15; //Sets large straight equal to 15
        }
      }
    }
  }
}
else if ( twos >= 2){ //Checks for large straight from 2-6, runs if roll is 2 3 4 5 6  in any order
  if (threes >= 3){
    if (fours >= 4){
      if (fives >= 5){
        if (sixes >= 6){
          lgst = 20; //Sets large straight equal to 20
        }
      }
    }
  }
}
else if ( aces >= 1){ //Checks for small straight from 1-4, runs if roll is 1 2 3 4 x in any oder
  if (twos >= 2){
    if (threes >= 3){
      if (fours >= 4){
        smst = 10; //Sets small straight equal to 10
      }
    }
  }
}
else if ( twos >= 2){ //Checks for small straight from 2-5, runs if roll is 2 3 4 5 x in any oder
  if ( threes >= 3){
    if ( fours >= 4){
      if (fives >= 5){
        smst = 14; //Sets small straight equal to 14
      }
    }
  }
}   
else if ( threes >= 3){ //Checks for small straight from 3-6, runs if roll is 3 4 5 6 x in any oder
  if (fours >= 4){
    if (fives >= 5){
      if (sixes >= 6){
        smst = 18; //Sets small straight equal to 18
      }
    }
  }
}
if ( smst > 0){ //Runs if small straight is greater than 0
  System.out.println("You got a small straight!"); //Prints "You got a small straight!"
}
if ( lgst > 0){ //Runs if large straight is greater than 0
  System.out.println("You got a large straight!"); ///Prints "You got a large straight!"
}
if (ytz == 0){ //Checks for combinations; Runs to see if there are values equal to zero for yahtzee, three of a kind, four of a kind, small straight, and large straights
  if (tok == 0){
    if (fok == 0){
      if (smst == 0){
        if (lgst == 0){
          System.out.println("You didn't have any combinations"); //Prints "You did not get any combinations"
        }
      }
    }
  }
}
chnc = die1 + die2 + die3 + die4 + die5; //Calculates chance value
if (tok > 0){ //Runs if three of a kind has a value greater than 0
  if (aces == 2){ //Checks for 2 1's
    fh = 2; //Sets full house equal to 2
  }
  if (twos == 4){ //Checks for 2 2's
    fh = 4; //Sets full house equal to 4
  }
  if (threes == 6){ //Checks for 2 3's
    fh = 6; //Sets full house equal to 6
  }
  if (fours == 8){ //Checks for 2 4's
    fh = 8; //Sets full house equal to 8
  }
  if (fives == 10){ //Checks for 2 5's
    fh = 10; //Sets full house equal to 10
  }
  if (sixes == 12){ //Checks for 2 6's
    fh = 12; //Sets full house equal to 12
  }
}
if (fh > 0){ //Runs if full house is greater than 0
  System.out.println("You got a full house!"); //prints "You got a full house!"
}
int upper = 0; //Creates variable for upper score
int finalupper = 0; //Creates variable for final upper score
int lower = 0; //Creates variable for lower score
int totalscore = 0; //Creates variable for total score
upper = aces + twos + threes + fours + fives + sixes; //Calculates upper score
if (upper > 63){ //Runs if upper score is greater than 63
    finalupper = upper + 35; //adds 35 to upper score for final upper score
}
  else if (upper <= 63){ //Runs is upper score is less than or equal to 63
      finalupper = upper; //sets upper score eual to final upper score
}
lower = tok + fok + fh + smst + lgst + ytz + chnc; //Calculates lower score
    System.out.println("The upper total is "+finalupper+""); //Prints upper score
    System.out.println("The lower total is "+lower+""); //Prints lower score
totalscore = finalupper+lower; //Calculates total score
    System.out.println("The final score is "+totalscore+""); //Prints total score
  } //End of class
}//end of main method