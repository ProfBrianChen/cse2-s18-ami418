import java.util.Random; //Imports Random
import java.util.Scanner; //Imports Scanner
public class lab07{ //Creates main class
public static String adjective(){ //creates method for generating an adjective
Random randomGenerator = new Random(); //Creates random generator
int randomInt = randomGenerator.nextInt(10); //Generated random integer from 1 to 10
String adjective = "a"; //Creates variable for adjective output
switch (randomInt){ //Switch statement for random int generating word
  case 0: adjective = "angry"; //Runs when randomInt is 0
    break;
  case 1: adjective = "boring"; //Runs when randomInt is 1
    break;
  case 2: adjective = "clumsy"; //Runs when randomInt is 2
    break;
  case 3: adjective = "devious"; //Runs when randomInt is 3
    break;
  case 4: adjective = "envious"; //Runs when randomInt is 4
    break;
  case 5: adjective = "foolish"; //Runs when randomInt is 5
    break;
  case 6: adjective = "gregarious"; //Runs when randomInt is 6
    break;
  case 7: adjective = "hungry"; //Runs when randomInt is 7
    break;
  case 8: adjective = "jealous"; //Runs when randomInt is 8
    break;
  case 9: adjective = "lazy"; //Runs when randomInt is 9
    break;  
  }
return adjective; //Returns adjective after being assigned a string
}
public static String noun(){ //creates method for generating an noun
Random randomGenerator = new Random(); //Creates random generator
int randomInt = randomGenerator.nextInt(10); //Generated random integer from 1 to 10
String noun = "a"; //Creates variable for noun output
switch (randomInt){ //Switch statement for random int generating word
  case 0: noun = "ant"; //Runs when randomInt is 0
    break;
  case 1: noun = "bear"; //Runs when randomInt is 1
    break;
  case 2: noun = "cat"; //Runs when randomInt is 2
    break;
  case 3: noun = "dog"; //Runs when randomInt is 3
    break;
  case 4: noun = "eagle"; //Runs when randomInt is 4
    break;
  case 5: noun = "fox"; //Runs when randomInt is 5
    break;
  case 6: noun = "gorilla";  //Runs when randomInt is 6
    break;
  case 7: noun = "horse"; //Runs when randomInt is 7
    break;
  case 8: noun = "koala"; //Runs when randomInt is 8
    break;
  case 9: noun = "lion"; //Runs when randomInt is 9
    break;  
  }
return noun; //Returns noun after being assigned a string
}
public static String verb(){ //creates method for generating an verb
Random randomGenerator = new Random(); //Creates random generator
int randomInt = randomGenerator.nextInt(10); //Generated random integer from 1 to 10
String verb = "a"; //Creates variable for verb output
switch (randomInt){ //Switch statement for random int generating word
  case 0: verb = "passed"; //Runs when randomInt is 0
    break;
  case 1: verb = "hated"; //Runs when randomInt is 1
    break;
  case 2: verb = "scratched"; //Runs when randomInt is 2
    break;
  case 3: verb = "moved"; //Runs when randomInt is 3
    break;
  case 4: verb = "pushed"; //Runs when randomInt is 4
    break;
  case 5: verb = "attacked"; //Runs when randomInt is 5
    break;
  case 6: verb = "swiped"; //Runs when randomInt is 6
    break;
  case 7: verb = "jumped"; //Runs when randomInt is 7
    break;
  case 8: verb = "raced"; //Runs when randomInt is 8
    break;
  case 9: verb = "fell over"; //Runs when randomInt is 9
    break;  
  }
return verb; //Returns verb after being assigned a string
}
public static String adverb(){ //creates method for adverb an adjective
Random randomGenerator = new Random(); //Creates random generator
int randomInt = randomGenerator.nextInt(10); //Generated random integer from 1 to 10
String adverb = "a"; //Creates variable for adverb output
switch (randomInt){ //Switch statement for random int generating word
  case 0: adverb = "quietly"; //Runs when randomInt is 0
    break;
  case 1: adverb = "peacefully"; //Runs when randomInt is 1
    break;
  case 2: adverb = "carefully"; //Runs when randomInt is 2
    break;
  case 3: adverb = "slowly"; //Runs when randomInt is 3
    break;
  case 4: adverb = "badly"; //Runs when randomInt is 4
    break;
  case 5: adverb = "closely"; //Runs when randomInt is 5
    break;
  case 6: adverb = "quickly";  //Runs when randomInt is 6
    break;
  case 7: adverb = "efficiently"; //Runs when randomInt is 7
    break;
  case 8: adverb = "painfully"; //Runs when randomInt is 8
    break;
  case 9: adverb = "secretly"; //Runs when randomInt is 9
    break;  
  }
return adverb; //Returns adverb after being assigned a string
}  
public static String presentverb(){ //creates method for verb an adjective
Random randomGenerator = new Random(); //Creates random generator
int randomInt = randomGenerator.nextInt(10); //Generated random integer from 1 to 10
String presentverb = "a"; //Creates variable for verb output
switch (randomInt){ //Switch statement for random int generating word
  case 0: presentverb = "mean to"; //Runs when randomInt is 0
    break;
  case 1: presentverb = "nice to"; //Runs when randomInt is 1
    break;
  case 2: presentverb = "sorry about"; //Runs when randomInt is 2
    break;
  case 3: presentverb = "annoyed by"; //Runs when randomInt is 3
    break;
  case 4: presentverb = "loathed by"; //Runs when randomInt is 4
    break;
  case 5: presentverb = "hated by"; //Runs when randomInt is 5
    break;
  case 6: presentverb = "passed by"; //Runs when randomInt is 6
    break;
  case 7: presentverb = "overtaken by"; //Runs when randomInt is 7
    break;
  case 8: presentverb = "swallowed by"; //Runs when randomInt is 8
    break;
  case 9: presentverb = "kicked by"; //Runs when randomInt is 9
    break;  
  }
return presentverb;  //Returns verb after being assigned a string
}
  public static void main(String[] args){ //Creates main method
String adjective1 = adjective(); //Generates adjective for adjective1
String adjective2 = adjective(); //Generates adjective for adjective2
String adjective3 = adjective(); //Generates adjective for adjective3
String noun1 = noun(); //Generates a noun
String verb1 = verb(); //generates a verb
String noun2 = noun(); //genrates a second noun
int i = 0; //creates int i = 0 for loops
while(i == 0){ //Creates loop for making sure words are not the same
  if (adjective1.equals(adjective2)){ //Runs if adjective 1 equals adjective 2
    adjective2 = adjective(); //generates new adjective2
  }
  if (!adjective1.equals(adjective2)){ //Runs if adjective 1 doesnt equal adjective 2
    i=1; //Stops loop
  }
}
i = 0; //sets i to 0
while(i == 0){
  if (adjective2.equals(adjective3)){ //Runs if adjective2 is the same as adjective3
    adjective3 = adjective(); //Gnerates new adjective3
  }
  if (!adjective2.equals(adjective3)){ //Runs if adjective2 and 3 and not the same
    i=1; //stops loop
  }
}
i = 0;
while(i == 0){ //Creates loop for making sure words are not the same
  if (noun1.equals(noun2)){ //Runs if nouns are the same
    noun1 = noun(); //genrated new noun
  }
  if (!noun1.equals(noun2)){ //runs if nouns arent the same
    i=1; //stops loop
  }
}
System.out.println("The "+adjective1+" "+adjective2+" "+noun1+" "+verb1+" the "+adjective3+" "+noun2+"."); //Prints first randomly generated sentence
Scanner myScanner = new Scanner(System.in); //Creates Scanner
System.out.print("Type 'yes' if you want another sentence: "); //Prompts user if they want another sentence
String input = myScanner.next(); //Creates variable from user input
String yes = "yes"; //Creates stirng variable to compare to user input
if (!input.equals(yes)){ //Runs if user types anyting but 'yes'
 System.exit(0); //Exits program
}
String adverb = adverb(); //Generates adverb
String verb2 = presentverb(); //Generates secomnd verb
String noun3 = noun(); //Genenrated third noun
for (i = 0; i==0;){ //Creates loop for making sure words are not the same
  if (noun3.equals(noun1) || noun3.equals(noun2)){ //Runs if nouns are the same
    noun3 = noun(); //Generates new noun3
  }
  else { //Runs if statement above is false
    i = 1; //stops loop
  }
}
String adjective4 = adjective(); //Generates fourth adjective
for (i = 0; i==0;){ //Creates loop for making sure words are not the same
  if (adjective4.equals(adjective1) || adjective4.equals(adjective2) || adjective4.equals(adjective3)){ //runs if adjective4 is the same as any other adjective
    adjective4 = adjective(); //Generates new adjective
  }
  else { //runs when if statement is false
    i = 1; //Stops loop
  }    
System.out.println("The "+noun1+" was "+adverb+" "+verb2+" the "+adjective4+" "+noun3+"." ); //Prints Second Sentence
  }
String verb3 = verb(); //Generates third verb
for (i = 0; i==0;){ //Creates loop for making sure words are not the same
  if (verb3.equals(verb1) || verb3.equals(verb2)){ //Runs if verb3 is the same as verb1 or verb2
    verb3 = verb(); //generated new verb
  }
  else { //runs when if statement is false
    i = 1; //Stops loop
  }  
}
System.out.println("The "+noun1+" "+verb3+" away from the "+noun3+"."); //Prints the final sentence
  }  //End of main method
} //End of class