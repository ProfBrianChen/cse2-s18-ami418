//Asher Illick-Frank ami418
//Argyle Patterns 3/20/18
//Sorry I know this does not print the desired output, I could not figure out how to print the argyle pattern correctly.
import java.util.Scanner; //Imports scanner
public class Argyle { //Creates class Argyle
  public static void main(String[] args){ //Creates main method
Scanner myScanner = new Scanner(System.in); //Creates scanner
String junkWord = "String"; //Creates throwaway scanner word
int width = 0; //Creates variable for width
int height = 0; //Creates variable for height
int argwidth = 0; //Creates variable for argyle width
int stripewidth = 0; //Creates variable for stripe width
char fill1 = 'a'; //Creates variable for back fill
char fill2 = 'a'; //Creates variable for diamonds
char fill3 = 'a'; //Creates variable for stripes
System.out.print("please enter a positive integer for the width of Viewing window in characters: "); //Prompts user input of width of veiwing window
for ( int i = 0; i == 0;){ //Loop for user input , runs when i is equal to 0
  if (myScanner.hasNextInt()){ //Runs if user input is an integer
    width = myScanner.nextInt(); //Sets course equal to user input if integer
    i = 1; //sets i to 1, ending the loop
    if (width < 0 ){ //Runs if user input is a negative integer
      i = 0; //Sets i to 0, continuing the looop
      System.out.print("Error, Negative, Please Enter an Positive Integer: "); //Tells user of error and prompts new user input
}
  }
    else { //Runs if the if statement above is false, so if user inputs anything other than integer
      junkWord = myScanner.next(); //Assigns junkword to get rid of previous user input
      System.out.print("Error, Not an Integer, Please Enter an Integer: "); //Tell user of error and prompts new user input
    }
}
System.out.print("please enter a positive integer for the height of Viewing window in characters: "); //Pompts user input of height of veiwing window
for ( int i = 0; i == 0;){ //Loop for user input , runs when i is equal to 0
  if (myScanner.hasNextInt()){ //Runs if user input is an integer
    height = myScanner.nextInt(); //Sets course equal to user input if integer
    i = 1; //sets i to 1, ending the loop
    if (height < 0 ){ //Runs if user input is a negative integer
      i = 0; //Sets i to 0, continuing the looop
      System.out.print("Error, Negative, Please Enter an Positive Integer: "); //Tells user of error and prompts new user input
}
  }
    else { //Runs if the if statement above is false, so if user inputs anything other than integer
      junkWord = myScanner.next(); //Assigns junkword to get rid of previous user input
      System.out.print("Error, Not an Integer, Please Enter an Integer: "); //Tell user of error and prompts new user input
    }
}
System.out.print("please enter a positive integer for the width of the argyle diamonds: "); //Prompts user input of width of argyle diamonds
for ( int i = 0; i == 0;){ //Loop for user input of width of argyle diamonds , runs when i is equal to 0
  if (myScanner.hasNextInt()){ //Runs if user input is an integer
    argwidth = myScanner.nextInt(); //Sets course equal to user input if integer
    i = 1; //sets i to 1, ending the loop
    if (argwidth < 0 ){ //Runs if user input is a negative integer
      i = 0; //Sets i to 0, continuing the looop
      System.out.print("Error, Negative, Please Enter an Positive Integer: "); //Tells user of error and prompts new user input
}
  }
    else { //Runs if the if statement above is false, so if user inputs anything other than integer
      junkWord = myScanner.next(); //Assigns junkword to get rid of previous user input
      System.out.print("Error, Not an Integer, Please Enter an Integer: "); //Tell user of error and prompts new user input
    }
}
System.out.print("please enter a positive odd integer for the width of the argyle center stripe: "); //Prompts user input of width of stripes
for ( int i = 0; i == 0;){ //Loop for user input of width , runs when i is equal to 0
  if (myScanner.hasNextInt()){ //Runs if user input is an integer
    stripewidth = myScanner.nextInt(); //Sets course equal to user input if integer
    i = 1; //sets i to 1, ending the loop
    if ((stripewidth < 0 || stripewidth % 2 == 0 ) || (argwidth/2 < stripewidth)){ //Runs if user input is a negative integer or even
      i = 0; //Sets i to 0, continuing the looop
      System.out.print("Error, Please Enter a positive odd Integer, must be less than half diamond size: "); //Tells user of error and prompts new user input
}
  }
    else { //Runs if the if statement above is false, so if user inputs anything other than integer
      junkWord = myScanner.next(); //Assigns junkword to get rid of previous user input
      System.out.print("Error, Not an Integer, Please Enter a positive odd Integer: "); //Tell user of error and prompts new user input
    }
}
System.out.print("please enter the first character for the pattern fill: "); //Prompts user input of back fill
String temp = myScanner.next(); //Assigns user input to a string
fill1 = temp.charAt(0); //Gets char variable from string for user input for back fill
System.out.print("please enter the second character for the pattern fill: "); //Prompts user input of diamond fill
temp = myScanner.next(); //Assigns user input to a string
fill2 = temp.charAt(0); //Gets char variable from string for user input for diamond fill
System.out.print("please enter the second character for the stripe fill: "); //Prompts user input of stripe fill
temp = myScanner.next(); //Assigns user input to a string
fill3 = temp.charAt(0);  //Gets char variable from string for user input for stripe fill
for (int j = 0; j < height; j++){ //Runs for every line
  for(int k = 0; k < width; k++){ //Runs for every column 
    if(j == k || k == (width)-(j+1)){ //runs under conditions for stripe
      for (int l= 0; l < stripewidth; l++) //Runs to create part of stripe with width
      System.out.print(""+fill3+""); //Prints Stripe 
    }
   else{ //Runs if the if statement above is false
     System.out.print(""+fill1+""); //Prints default fill
   }    
 }
  System.out.println(); //Moves to next line after loop is finished
}
} //End of main method
  } //End of class