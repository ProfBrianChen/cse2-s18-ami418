//ami418 Asher Illick-Frank
//lab04 2/16/18

import java.util.Random;
public class CardGenerator {
  public static void main (String[] args) {
String suit = "a"; //Creates variable for suit
String number = "Ace"; //Creates variable for 
Random pick = new Random(); //Generates random number from 1 to 52
  int  card = pick.nextInt(52) + 1;
if (card <= 13){ //Runs if the card number is less than or equal to 13
  suit = "Clubs"; //Sets suit to clubs
}
else if (card <= 26){  //Runs if the card number is less than or equal to 26 and greater than 13
  suit = "Hearts"; //Sets suit to hearts
}
else if (card <= 39){ //Runs if the card number is less than or equal to 39 and greater than 26
  suit = "Spades"; //Sets suit to spades
}
else if (card <= 52){ //Runs if the card number is less than or equal ot 52 and greater than 39
   suit = "Diamonds"; //Sets suit to diamonds
} 
switch( card % 13 ) { //Switch that uses the modulus operator to determine the car value from ace to king
  case 1: //if the remainder is equal to 1, sets number to ace
    number = "Ace";
    break;
  case 2: //if remainder is equal to 2, sets number to 2
    number = "2";
    break;
  case 3: //if remainder is equal to 3, sets number to 3
    number = "3";
    break;
  case 4: //if remainder is equal to 4, sets number to 4
    number = "4";
    break;
  case 5: //If remainder is equal to 5, sets number to 5
    number = "5";
    break;
  case 6: //If remainder is equal to 6, sets number to 6
    number = "6";
    break;
  case 7: //If remainder is equal to 7, sets number to 7
    number = "7";
    break;
  case 8: //If remainder is equal to 8, sets number to 8
    number = "8";
    break;
  case 9: //If remainder is equal to 9, sets number to 9
    number = "9";
    break;
  case 10: //If remainder is equal to 10, sets number to 10
    number = "10";
    break;
  case 11: //If remainder is equal to 11, sets number to jack
    number = "Jack";
    break;  
  case 12: //If remainder is equal to 12, sets number to queen
    number = "Queen";
    break;
  case 0: //If remainder is equal to 0, sets number to king
    number = "King";
    break;
}
  System.out.println("You picked the "+number+" of "+suit+""); //Prints the card selected
	
    }//End of main class
}//End of main method