//CSE2
//Asher Illick-Frank 
//ami418 4/17/18
//hw09 DrawPoker.java
import java.util.*; //imports all utilities
public class DrawPoker { //start of class
public static void main(String[] args){ //start of main method
int[] deck = new int[52]; //creates int array and allocates space
for (int i = 0; i < deck.length; i++){ //loop that runs when i is less than deck length
  deck[i] = i+1; //numbers cards 1-52
}
deck = Shuffler(deck); //calls shuffle method and shuffles deck
int[] hand1 = {deck[0], deck[2], deck[4], deck[6], deck[8]}; //"deals" hand 1
int[] hand2 = {deck[1], deck[3], deck[5], deck[7], deck[9]}; //deals hand 2
boolean flushhand1 = flush(hand1); //calls flush method to check if hand 1 has flush
boolean flushhand2 = flush(hand2); //calls flush method to check if hand 2 has flush
int card1 = hand1[0]; //copies int in array to singular int
int card2 = hand1[1]; //copies int in array to singular int
int card3 = hand1[2]; //copies int in array to singular int
int card4 = hand1[3]; //copies int in array to singular int
int card5 = hand1[4]; //copies int in array to singular int
int card6 = hand2[0]; //copies int in array to singular int
int card7 = hand2[1]; //copies int in array to singular int
int card8 = hand2[2]; //copies int in array to singular int
int card9 = hand2[3]; //copies int in array to singular int
int card10 = hand2[4]; //copies int in array to singular int
String scard1 = card(card1); //calls card method to return name of card
String scard2 = card(card2); //calls card method to return name of card
String scard3 = card(card3); //calls card method to return name of card
String scard4 = card(card4); //calls card method to return name of card
String scard5 = card(card5); //calls card method to return name of card
String scard6 = card(card6); //calls card method to return name of card
String scard7 = card(card7); //calls card method to return name of card
String scard8 = card(card8); //calls card method to return name of card
String scard9 = card(card9); //calls card method to return name of card
String scard10 = card(card10); //calls card method to return name of card
System.out.println("The first hand is "+scard1+", "+scard2+", "+scard3+", "+scard4+", and "+scard4+"."); //Print hand 1
System.out.println("The second hand is "+scard6+", "+scard7+", "+scard8+", "+scard9+", and "+scard10+"."); //Prints hand 2
hand1 = number(hand1); //calls numbe rmethod to cahnge 1-52 to 1-13
hand2 = number(hand2); //calls numbe rmethod to cahnge 1-52 to 1-13
boolean pairhand1 = pair(hand1); //Calls pair method to return boolean if pair is found
boolean pairhand2 = pair(hand2); //Calls pair method to return boolean if pair is found
boolean triplehand1 = triple(hand1); //Calls triple method to return boolean if three of a kind are found
boolean triplehand2 = triple(hand2); //Calls triple method to return boolean if three of a kind are found
boolean fullhousehand1 = fullhouse(hand1); //Calls fullhouse method to return boolean if three of a kind and a pair are found
boolean fullhousehand2 = fullhouse(hand2);  //Calls fullhouse method to return boolean if three of a kind and a pair are found
int highcardhand1 = highcard(hand1); //Calls highcard method and reeturns the int of highest card
int highcardhand2 = highcard(hand2); //Calls highcard method and reeturns the int of highest card
if (flushhand1 && !flushhand2){ //Runs if only hand 1 has flush
	System.out.println("Hand 1 has a flush and wins."); //Tell user hand 1 wins
	System.exit(0); //Exits program
}
if (flushhand2 && !flushhand1){ //Runs if only hand 2 has flush
	System.out.println("Hand 2 has a flush and wins."); //Tell user hand 2 wins
	System.exit(0); //Exits program
}
if (flushhand1 && flushhand2){ //Runs if both hands have flush
	System.out.println("Both hand 1 and hand 2 have a flush and tie."); //Tells user that hands tie
	System.exit(0); //Exits program
}
if (fullhousehand1 && !fullhousehand2){ //Runs if only hand 1 has a full house
	System.out.println("Hand 1 has a flush and wins."); //Tell user hand 1 wins
	System.exit(0); //Exits program
}
if (fullhousehand2 && !fullhousehand1){ //Runs if only hand 2 has a full house
	System.out.println("Hand 2 has a flush and wins."); //Tell user hand 2 wins
	System.exit(0); //Exits program
}
if (fullhousehand1 && fullhousehand2){ //Runs if both hands have a full house
	System.out.println("Both hand 1 and hand 2 have a flush and tie."); //Tells user that hands tie
	System.exit(0); //Exits program
}
if (triplehand1 && !triplehand2){ //Runs if oly hand 1 has three of kind
	System.out.println("Hand 1 has three of a kind and wins."); //Tell user hand 1 wins
	System.exit(0); //Exits program
}
if (triplehand2 && !triplehand1){ //runs if only hand 2 has three of a kind
	System.out.println("Hand 2 has three of a kind and wins."); //Tell user hand 2 wins
	System.exit(0); //Exits program
}
if (triplehand1 && triplehand2){ //Runs if both hands hanve three of a kind
	System.out.println("Both hand 1 and hand 2 have three of a kind and tie."); //Tells user that hands tie
	System.exit(0); //Exits program
}
if (pairhand1 && !pairhand2){ //Runs if only hand 1 has a pair
	System.out.println("Hand 1 has a pair and wins."); //Tell user hand 1 wins
	System.exit(0); //Exits program
}
if (pairhand2 && !pairhand1){ //Runs if only hand 1 has a pair
	System.out.println("Hand 2 has a pair and wins."); //Tell user hand 2 wins
	System.exit(0); //Exits program
}
if (pairhand1 && pairhand2){ //Runs if both hands have a pair
	System.out.println("Both hand 1 and hand 2 have pairs and tie."); //Tells user that hands tie
	System.exit(0); //Exits program
}
if (highcardhand1 > highcardhand2){
	System.out.println("Hand 1 has the highest card and wins."); //Tell user hand 1 wins
	System.exit(0); //Exits program
}
if (highcardhand2 > highcardhand1){
	System.out.println("Hand 2 has the highest card and wins."); //Tell user hand 2 wins
	System.exit(0); //Exits program
}
if (highcardhand1 == highcardhand2){
	System.out.println("Both hands have the same high card and tie."); //Tells user that hands tie
	System.exit(0); //Exits program
}
} //End of main method
public static void print(int array[]){ //Creates method for printing arrays
String out1="The hand is ";  //Sets first string equal to "The hand is"
String out="{"; //adds { to second string
for(int j=0;j<array.length;j++){ //Loop that runs when j is less than array length, post incremented
  if(j>0){ //runs if j is greater than 0
    out+=", "; //adds comma to second string
  }
out+=array[j]; //adds int of index j to second string
}
out+="} "; //adds } to second string
System.out.println(out1+out); //Prints both strings
} //End of print method
public static int[] Shuffler(int[] array){ //creates method for shuffling array
Random random = new Random();  //Random number generator			
for (int i=0; i<array.length; i++) { //runs when i is less than the length of the array array
	int randomPosition = random.nextInt(array.length); //generates a random position
	int holder = array[i]; //sets holder to array index i
	array[i] = array[randomPosition]; //sets array idex i to the array of index of random
	array[randomPosition] = holder; //sets array idex of the random position to the holder
} 
return array; //returns array array
} //End of shuffler method
public static String card(int card){ 
String suit = ""; //Creates empty string for suit
String number = ""; //Creates empty string for number
if (card <= 13){ //Runs if the card number is less than or equal to 13
  suit = "Clubs"; //Sets suit to clubs
}
else if (card <= 26){  //Runs if the card number is less than or equal to 26 and greater than 13
  suit = "Hearts"; //Sets suit to hearts
}
else if (card <= 39){ //Runs if the card number is less than or equal to 39 and greater than 26
  suit = "Spades"; //Sets suit to spades
}
else if (card <= 52){ //Runs if the card number is less than or equal ot 52 and greater than 39
   suit = "Diamonds"; //Sets suit to diamonds
} 
switch( card % 13 ) { //Switch that uses the modulus operator to determine the car value from ace to king
  case 1: //if the remainder is equal to 1, sets number to ace
    number = "Ace"; 
    break;
  case 2: //if remainder is equal to 2, sets number to 2
    number = "2"; 
    break;
  case 3: //if remainder is equal to 3, sets number to 3
    number = "3";
    break;
  case 4: //if remainder is equal to 4, sets number to 4
    number = "4";
    break;
  case 5: //If remainder is equal to 5, sets number to 5
    number = "5";
    break;
  case 6: //If remainder is equal to 6, sets number to 6
    number = "6";
    break;
  case 7: //If remainder is equal to 7, sets number to 7
    number = "7";
    break;
  case 8: //If remainder is equal to 8, sets number to 8
    number = "8";
    break;
  case 9: //If remainder is equal to 9, sets number to 9
    number = "9";
    break;
  case 10: //If remainder is equal to 10, sets number to 10
    number = "10";
    break;
  case 11: //If remainder is equal to 11, sets number to jack
    number = "Jack";
    break;  
  case 12: //If remainder is equal to 12, sets number to queen
    number = "Queen";
    break;
  case 0: //If remainder is equal to 0, sets number to king
    number = "King";
    break;
} //Find number value of card
String stringreturn = "the "+number+" of "+suit+""; //Sets string to card value
return stringreturn; //returns string
  } //End of card method
public static int[] number(int card[]){ //Creates method for getting number of card
int[] number = new int[5]; //creates new array of length 5
for (int i = 0; i < card.length; i++){ //loop that runs when i is less than card length
switch( card[i] % 13 ) { //Switch that uses the modulus operator to determine the car value from ace to king
  case 1: //if the remainder is equal to 1, sets number to ace
    number[i] = 1;
    break;
  case 2: //if remainder is equal to 2, sets number to 2
    number[i] = 2;
    break;
  case 3: //if remainder is equal to 3, sets number to 3
    number[i] = 3;
    break;
  case 4: //if remainder is equal to 4, sets number to 4
    number[i] = 4;
    break;
  case 5: //If remainder is equal to 5, sets number to 5
    number[i] = 5;
    break;
  case 6: //If remainder is equal to 6, sets number to 6
    number[i] = 6;
    break;
  case 7: //If remainder is equal to 7, sets number to 7
    number[i] = 7;
    break;
  case 8: //If remainder is equal to 8, sets number to 8
    number[i] = 8;
    break;
  case 9: //If remainder is equal to 9, sets number to 9
    number[i] = 9;
    break;
  case 10: //If remainder is equal to 10, sets number to 10
    number[i] = 10;
    break;
  case 11: //If remainder is equal to 11, sets number to 11
    number[i] = 11;
    break;  
  case 12: //If remainder is equal to 12, sets number to 12
    number[i] = 12;
    break;
  case 0: //If remainder is equal to 0, sets number to 14
    number[i] = 13;
    break;
}
}
return number; //returns number
} //End of number method
public static boolean pair(int array[]){ //Creates method for cehcking if there is a pair
boolean pair = false; //creates and sets boolean pair to false
for (int j = 0; j < array.length; j++){ //loop that runs when j is less than array length
  for (int i = 0; i < array.length; i++){ //loop that runs when i is less than array length
    if (array[0] == array[i]){ //runs if int at index 0 is equal to int at index i
      if (i != 0){ //Runs if i is not 0
        pair = true; //Sets pair to true
      }
    }
    if (array[1] == array[i]){ //runs if int at index 1 is equal to int at index i
      if (i != 1){ //Runs if i is not 1
        pair = true; //Sets pair to true
      }
    }
    if (array[2] == array[i]){ //runs if int at index 2 is equal to int at index i
      if (i != 2){ //Runs if i is not 2
        pair = true; //Sets pair to true
      }
    }
    if (array[3] == array[i]){ //runs if int at index 3 is equal to int at index i
      if (i != 3){ //Runs if i is not 3
        pair = true; //Sets pair to true
      }
    }
     if (array[4] == array[i]){ //runs if int at index 4 is equal to int at index i
      if (i != 4){ //Runs if i is not 4
        pair = true; //Sets pair to true
      }
    }
  }
}
return pair; //Returns pair
} //End of pair method
public static boolean flush(int array[]){ //Creates method for checking if hand has a flush
boolean flush = false; //creates boolean for flush and sets to false
int[] quotient = new int[5]; //creates new array of length 5
for (int i = 0; i < array.length; i++){ //runs when i is less than array length
	if (array[i] <= 13){ //Runs if the card number is less than or equal to 13
		quotient[i] = 0; //Quotient index i gets set to 0
}
else if (array[i] <= 26){  //Runs if the card number is less than or equal to 26 and greater than 13
		quotient[i] = 1; //Quotient index i gets set to 0
}
else if (array[i] <= 39){ //Runs if the card number is less than or equal to 39 and greater than 26
		quotient[i] = 2; //Quotient index i gets set to 0
}
else if (array[i] <= 52){ //Runs if the card number is less than or equal ot 52 and greater than 39
		quotient[i] = 3; //Quotient index i gets set to 0
} 
}
if (quotient[0] == quotient[1] && quotient[1] == quotient[2] && quotient[2] == quotient[3] && quotient[3] == quotient[4]){ //Runs if all cards of same suit
	flush = true; //sets flush to true
}
return flush; //returns flush
} //End of flush method
public static boolean triple(int array[]){ //Creates method for checking if there are three of kind
boolean triple = false; //creates booleam for triple and sets to false
if (array[0] == array[1] && array[1] == array[2]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
} 
if (array[0] == array[1] && array[1] == array[3]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
}
if (array[0] == array[1] && array[1] == array[4]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
} 
if (array[1] == array[2] && array[2] == array[3]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
} 
if (array[1] == array[2] && array[2] == array[4]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
} 
if (array[1] == array[2] && array[2] == array[0]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
} 
if (array[0] == array[1] && array[1] == array[2]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
} 
if (array[2] == array[3] && array[3] == array[0]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
} 
if (array[2] == array[3] && array[3] == array[4]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
}  
if (array[3] == array[4] && array[4] == array[0]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
} 
if (array[3] == array[4] && array[4] == array[1]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
} 
if (array[2] == array[4] && array[4] == array[1]){ //Runs if for one possible combination of three of a kind are the same
	triple = true; //Sets triple to true
}
return triple; //Returns triple
} //End of triple method
public static int triplevalue(int array[]){ //Creates method for finding value of triple
int triple = 0; //creates int variable for triple
if (array[0] == array[1] && array[1] == array[2]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[0]; //Sets triple to int at index 0
} 
if (array[0] == array[1] && array[1] == array[3]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[0]; //Sets triple to int at index 0
}
if (array[0] == array[1] && array[1] == array[4]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[0]; //Sets triple to int at index 0
} 
if (array[1] == array[2] && array[2] == array[3]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[1]; //Sets triple to int at index 1
} 
if (array[1] == array[2] && array[2] == array[4]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[1]; //Sets triple to int at index 1
} 
if (array[1] == array[2] && array[2] == array[0]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[1]; //Sets triple to int at index 1
} 
if (array[2] == array[3] && array[3] == array[0]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[2]; //Sets triple to int at index 2
} 
if (array[2] == array[3] && array[3] == array[4]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[2]; //Sets triple to int at index 2
}  
if (array[3] == array[4] && array[4] == array[0]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[3]; //Sets triple to int at index 3
} 
if (array[3] == array[4] && array[4] == array[1]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[3]; //Sets triple to int at index 3
} 
if (array[2] == array[4] && array[4] == array[1]){ //Runs if for one possible combination of three of a kind are the same
	triple = array[2]; //Sets triple to int at index 2
}
return triple; //returns int triple
} //End of method triplevalue
public static boolean fullhouse(int array[]){ //Creates methof for determing full house
boolean fullhouse = false; //Creates boolean for full house and sets to false
int triplevaluehand = 0; //creates int for value of three of a kind
int pairvalue = 0; //creates int for value of pair
boolean pairhand = pair(array); //creates boolean and checks if array has a pair
boolean triplehand = triple(array); //creates boolean and checks if array has three of a kind
if (triplehand){ //runs if there is three of kind
	triplevaluehand = triplevalue(array); //calls method to check if array has triple and returns int of what number
}
if (pairhand){ //runs if there are two of a kind
	pairvalue = pairvalue(array); //calls method and returns with int of pair
}
if (pairhand && triplehand){ //runs if hand has both double and triple
	if (pairvalue != triplevaluehand){ //runs if pair is not equal to three of a kind int
		fullhouse = true; //sets full house to true
	}
}
return fullhouse; //returns fullhouse
} //End of fullhouse method
public static int highcard(int array[]){ //Creates method for getting the highest card value
int highcard = array[0]; //creates int variable set equal intially to array index 0
for (int i = 0; i < array.length; i++){ //runs when i is less than the length of the array
	if (array[i] > highcard){ //runs if array index i is greater than highcard
		highcard = array[i]; //Sets array index i to highcard and repeats
	}
}
for (int i = 0; i < array.length; i++){ //runs when i is less than length of array
	if (array[i] == 1){ //runs when number in array is 1
		highcard = 14; //sets highcard to 14, because aces are the highest card
	}
}
return highcard; //returns highcard
} //End of highcard method
public static int pairvalue(int array[]){ //Creates method for determining value of pair
int pair = 0; //creates int for pair sets equal to 0
for (int j = 0; j < array.length; j++){ //runs when j is less than array length
  for (int i = 0; i < array.length; i++){ //runs when i is less than array length
    if (array[0] == array[i]){ //runs if int at index 0 is equal to int at index i
      if (i != 0){ //Runs if i is not 0
					pair = array[0]; //Sets pair to value of array at index 0
				}
      }
    if (array[1] == array[i]){ //runs if int at index 1 is equal to int at index i
      if (i != 1){ //Runs if i is not 1
					pair = array[1]; //Sets pair to value of array at index 1
				}
      }
    if (array[2] == array[i]){ //runs if int at index 2 is equal to int at index i
      if (i != 2){ //Runs if i is not 2
					pair = array[2]; //Sets pair to value of array at index 2
      }
    }
    if (array[3] == array[i]){ //runs if int at index 3 is equal to int at index i
      if (i != 3){ //Runs if i is not 3
					pair = array[3]; //Sets pair to value of array at index 3
      }
    }
     if (array[4] == array[i]){ //runs if int at index 4 is equal to int at index i
      if (i != 4){ //Runs if i is not 4
					pair = array[4]; //Sets pair to value of array at index 4

      }
    }
  }
}
return pair; //returns pair
} //End of pairvalue method
} //End of class