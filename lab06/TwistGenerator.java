//Asher Illick-Frank ami418
//lab05 TwistGenerator
import java.util.Scanner; //Imports scanner
public class TwistGenerator { //Creates main class TwistGenerator
public static void main(String[] args){ //Creates main method
Scanner myScanner = new Scanner(System.in); //Creates Scanner
int i = 0; //Creates variable i for loops
int userinput = 0; //Creates variable for user input
String junkWord = "a"; //creates throw away junkWord for invalid user inputs
System.out.print("Enter a Positive Integer: "); //Prompts user to input an integer
while (i == 0){ //Loop for determining valid user input
  if (myScanner.hasNextInt()){ //Runs if user input is an integer
    userinput = myScanner.nextInt(); //sets i 
    i = 1; //Sets i to 1 ending loop if not changed
    if (userinput <= 0){ //runs if integer is negative
      i = 0; //Sets i to 0 continuing loop
      System.out.print("Error, Negative, Please Enter a Positive Integer: "); //Tells user of error and prompts new user input
}
  }
    else { //Runs if previous if statement is false
      junkWord = myScanner.next(); //Assigns junkword to get rid of previous user input
      System.out.print("Error, Not an Integer, Please Enter an Integer: "); //Tells user of error and prompts new user input
    }
}
int length = userinput; //Creates variable for length of twist
int top = length; //Sets top of twist equal to length
int top1 = top/3; //Sets computational variable for slashes equal to top divided by 3
while (top1 > 0){ //Loop for top line of twist, runs when top1 is greater than 0
  System.out.print("\\ /"); //Prints "\ /" for every 3 number
    top1--; //post decrement top1 after printing , prints "\ /" for every 3 original input numbers
}
switch (top % 3){ //Switch using top modulo 3 to get additional slashes in top line
  case 1: //Runs if top % 3 is 1
    System.out.print("\\"); //Prints \"
    break; 
  case 2: //Runs if top % 3 is 2
    System.out.print("\\ ");//Prints "\ "
    break;
}
  System.out.println(); //Moves print output to next line
int xs = length/3; //Sets xs equal to length divided by 3
while ( xs > 0){ //loop for xs, runs while xs greater than 0
  System.out.print(" X "); //Prints " X " for every 3 numbers
  xs--; //post decrement after printing, prints X for every 3 original input numbers
}
System.out.println(); //Moves print output to next line
int bottom = length; //Sets bottom of twist equal to length
int bottom1 = bottom/3; //Sets computational variable for slahses equal to bottom divided by 3
while (bottom1 > 0){ //Loop  for bottom line of twitst, runs when bottom1 is greater than 0
  System.out.print("/ \\"); //Prints "/ \" for every 3 numbers
    bottom1--; //post decrement bottom1 after prinitng, prints "/ \" for every 3 orgianl input numbers
}
switch (bottom % 3){ //Switch using bottom modulo 3 to get additional slashes in bottom line
  case 1: //Runs if bottom % 3 is 1
    System.out.print("/"); //Prints "/"
    break;
  case 2: //Runs if bottom % 3 is 2
    System.out.print("/ "); //Prints "/ "
    break;
}
System.out.println(); //Moves print output to next line
  }//End of class
}//End of main method
