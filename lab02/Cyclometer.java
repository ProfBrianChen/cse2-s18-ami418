//ami418 Asher Illick-Frank 2/2/18 lab02
// Cyclometer for trip//Practice assigning, declaring, and calculating variables
// Determine, time, rotations, and distance traveled for two trips.
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      // our input data. Document your variables by placing your
      // Assigning an identifier as the variable of length of trip 1 in seconds, and declaring the variable as an integer valued at 480.
	  int secsTrip1=480;
      // Assigning an identifier as the variable of length of trip 2 in seconds, and declaring the variable as an integer valued at 3220.
    int secsTrip2=3220;
      // Assigning an identifier as the variable of rotations of trip 1 and declaring the variable as an integer valued at 1561.
		int countsTrip1=1561;
      // Assigning an identifier as the variable of rotations of trip 1 and declaring the variable as an integer valued at 1561.
		int countsTrip2=9037; 
// our intermediate variables and output data. Document!
      // Assigns and declares the variable wheelDiameter a double equal to 27.0
    double wheelDiameter=27.0, 
      // Assigning what the variable Pi equal the value of pi, 3.14159.
  	  PI=3.14159,
      // Assigning the variable feetPerMile equal to the value of feet per mile, 12.
  	  feetPerMile=5280,
      // Assigning the variable inchesPerFoot equal to the value of inches per foot, 60.
  	  inchesPerFoot=12,
      // Assigning the variable of secondsPerMinute seconds per minute
  	  secondsPerMinute=60;
      // declaring type of variable, doubles for the four variables above.
	  double distanceTrip1, distanceTrip2,totalDistance; 
      // Prints how many minutes and rotation in trip 1 
      System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+ " counts.");
      // Prints how many minutes and rotation in trip 2
	    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
      // Calculating trip distance by multiplyuing rotations by wheel diameter and pi, calculating the circumference and multiplying by the rotations per trip.
// Above gives distance in inches
	    distanceTrip1=countsTrip1*wheelDiameter*PI;
//(for each count, a rotation of the wheel travels the diameter in inches times PI)
      // Calculates distance of trip1 in miles
	    distanceTrip1/=inchesPerFoot*feetPerMile;
      // Calculates distance of trip2 in miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
      // Calculates the total distance by adding the distances of trip1 and trip2
	    totalDistance=distanceTrip1+distanceTrip2;
//Print out the output data.
      // Prints distance of trip 1
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
      // Prints distance of trip 2
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
      // Prints total trip distance
	    System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class
