/////////////////////////////
//CSE 2 WelcomeClass
///
public class WelcomeClass {
     public static void main(String[] args) {     
          System.out.println("  -----------");
          System.out.println("  | WELCOME |");
          System.out.println("  -----------");
          System.out.println("  ^  ^  ^  ^  ^  ^");
          System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
          System.out.println("<-A--M--I--4--1--8->");
          System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
          System.out.println("  v  v  v  v  v  v");   
     }
}