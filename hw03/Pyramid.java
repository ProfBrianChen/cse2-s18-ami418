//ami418 Asher Illick-Frank
//hw03 2/13/18
//Imports Scanner
import java.util.Scanner;
//Main method required for every java program
public class Pyramid {
  public static void main(String[] args){
    //Specifies scanner for variables
Scanner myScanner;
    //Creates Scanner
myScanner = new Scanner( System.in );
    //Alows user to inbut the length of a sqaure side
    System.out.print("The square side of the pyramid is (input length): "); 
    //Creates variable of user input of length
double base = myScanner.nextDouble();
    System.out.print("The height of the pyramid is (input height): ");
    //Creates variable of user input height
double height = myScanner.nextDouble();
    //Calculates volume of pryamid
double volume = (base*base*height)/3;
    //Prints volume of pyramid
    System.out.println("The volume inside the pyramid is "+volume+"");
  } //End of main method
} //End of class
