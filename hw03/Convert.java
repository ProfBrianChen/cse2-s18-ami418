//ami418 Asher Illick-Frank
//hw03 2/13/18
//Imports Scanner
import java.util.Scanner;
//Main method required for every java program
public class Convert { 
  public static void main(String[] args) {
    //Specifies scanner for variables
Scanner myScanner; 
    //Creates scanner
myScanner = new Scanner( System.in );
    //Allows user to input area
    System.out.print("Enter the affected area in acres: "); 
    //Creates variable of user input of area
double acres = myScanner.nextDouble(); 
    //Allows user to input rainfall
    System.out.print("Enter the rainfall in the affected area: ");
    //Creates variable of user input of rainfall
double rainfall = myScanner.nextDouble();
    //Calculates area in miles
double areamiles = acres*0.0015625; 
    //Calculates rainfall in miles
double rainfallmiles = rainfall*0.000015783; 
    //Calculates cubic miles
double cubicmiles = areamiles*rainfallmiles; 
    //Prints cubic miles
  System.out.println(""+cubicmiles+" cubic miles.");
  }  //end of main method
} //end of class