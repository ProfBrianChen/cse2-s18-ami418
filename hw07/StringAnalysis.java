import java.util.Scanner; //Imports scanner
public class StringAnalysis { //Creates main class
public static boolean analysis(String input){ //Creates method analysis taking singular string input, used if all of string examined, outputs a boolean
int finallength = input.length(); //Finds length of string user entered
boolean finalanswer = true; //Creates true boolean for final answer, the default output if nothing is changed
for (int i = 0; i < finallength; i++){ //Creates loop, int i equal to zero, runs if i is less than final length, post incremented
  if (!Character.isLetter(input.charAt(i))){ //Runs if character is not a letter, finds char in string and returns true if letter, hence the use of !
  finalanswer = false; //Sets final answer to false, and the output will be false
  i = finallength; //Sets i to final length, stopping the loop
  }
}
return finalanswer; //returns final answer true if all letters, false if not
  } //End of method analysis
public static boolean analysis(String input, int userlength){ //Creates method analysis taking a string input and a int input, used for examining part of string, outputs a boolean
int finallength = 0; //Creates variable for final length, the one used in the loop for determining type, defaults to 0 util changed
boolean finalanswer = true; //Creates true boolean for final answer, the default output if nothing is changed
int stringlength = input.length(); //Finds length of string user entered
if (userlength >= stringlength){ //Runs if user inputed length is greater than actual string length
  finallength = stringlength; //Sets final length, one used in loop, to the length of the string
}
if (stringlength > userlength){ //Runs if user inputed loop is smaller than the actual string length
  finallength = userlength; //Sets final length, one used in loop, to the length of the string
}  
for (int i = 0; i < finallength; i++){ //Creates loop, int i equal to zero, runs if i is less than final length, post incremented
  if (!Character.isLetter(input.charAt(i))){ //Runs if character is not a letter, finds char in string and returns true if letter, hence the use of !
  finalanswer = false; //Sets final answer to false, and the output will be false
  i = finallength; //Sets i to final length, stopping the loop
  }
}
return finalanswer; //returns final answer true if all letters, false if not
} //End of method analysis
public static void main(String[] args){ //Creates main method
System.out.print("Enter a string: "); //Prompts user to enter a string
Scanner myScanner = new Scanner(System.in); //Creates Scanner for main method
String input = myScanner.next(); //Creates variable for input and sets equal to user input
String subset = "a"; //Creates a variable for user input if they want entrire strong checked or only a smaller number
String yes = "yes"; //Creates a string for "yes" used for checking input
String no = "no"; //Creates a string for "no" used for checking input
int length = 0; //Creates int variable for length of the string
String junkWord = "a"; //Creates throwaway string for invalid user input
System.out.print("If you wish to examine all the characters type 'yes' or type 'no' if you wish to only examine only a certain number: "); //Prompts user to pick if they want all numbers examined, or just a smaller number
for (int i = 0; i==0;){ //Creates loop that runs when i is equal to 0
  subset = myScanner.next(); //Sets user input to the variable subset
  if (subset.equals(no)){ //Runs if user input is "no"
    System.out.print("Enter the desired length of your string to be examined as an integer: ");
     for (int j = 0; j==0;){ //Creates loop that runs when j = 0
       if (myScanner.hasNextInt()){ //Runs if user input is an integer
         length = myScanner.nextInt(); //Sets length equal to user input
         System.out.print("Examining the first "+length+" characters."); //Tells user that is examining the number of charcterts they selected
         j = 1; //sets j equal to 1, stopping the loop
         if (length <= 0){ //Runs if user input is negative
           j = 0; //Sets j equal to 0, continuing the loop
           System.out.print("Please enter a positive integer: "); //Prompts user to enter a positive integer
         }
       }
       else { //Runs if an integer is not entered
         junkWord = myScanner.next(); //takes user input and sets it equal to a throwaway word
         System.out.print("Please enter an positive integer: "); //prompts use to enter a positive integer
       }
     }
    i = 1; //sets i equal to 1, ending the loop
  }
  if (subset.equals(yes)){ //Runs if user input is "yes"
    System.out.print("Examining all characters."); //Tell user that all characters are being examined
    i = 1; //sets i equal to 1, ending the loop
  }
  if (!subset.equals(no) & !subset.equals(yes)){ //Runs if user input is neither "yes" or "no"
    System.out.print("Invalid input, please respond with 'yes' or 'no': "); //Prompts user for yes or no input for program to run as loop continues
  }
}
System.out.println(); //Prints line
boolean finalanswer = true; //creates boolean value for finalanswer in main method, defaults to true
if (length == 0){ //Runs if user wants all of string examined
  finalanswer = analysis(input); //calls method, and sets final answer equal to the output
}
if (length > 0){ //Runs if user inputed a specific length examined
  finalanswer = analysis(input, length); //calls method, and sets final answer equal to the output
}
if (finalanswer){ //Runs if final answer is true, meaning all the characters are letters
  System.out.println("The characters examined contains only letters."); //Tells user that string contains only letter
}
if (!finalanswer){ //Runs if final answer is false, measning there are charracters other than letters
  System.out.println("The characters examined contains other characters besides letters."); //Tells user that the string has other characters besides letters
}
  } //End of main method
} //End of class