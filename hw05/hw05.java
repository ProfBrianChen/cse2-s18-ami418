//Asher Illick-Frank ami418
//hw06 Writing with Loops
import java.util.Scanner; //Imports Scanner Utility
public class hw05 { //Creates main class hw05
  public static void main(String[] args){ //Creates main method
Scanner myScanner = new Scanner(System.in); //Creates scanner
int i = 0; //Creates variable i for loops
String junkString = "String"; //Creates throwaway scanner word
int course = 0; //Creates int variable for course number
  System.out.print("Course Number: "); //Prompts user to input course number
while (i == 0){ //Loop for course number, runs when i is equal to 0
  if (myScanner.hasNextInt()){ //Runs if user input is an integer
    course = myScanner.nextInt(); //Sets course equal to user input if integer
    i = 1; //sets i to 1, ending the loop
    if (course <= 0){ //Runs if user input is a negative integer
      i = 0; //Sets i to 0, continuing the looop
      System.out.print("Error, Negative, Please Enter an Positive Integer: "); //Tells user of error and prompts new user input
}
  }
    else { //Runs if the if statement above is false, so if user inputs anything other than integer
      junkString = myScanner.next(); //Assigns junkword to get rid of previous user input
      System.out.print("Error, Not an Integer, Please Enter an Integer: "); //Tell user of error and prompts new user input
    }
}
String coursefixer = ""; //Creates string to fix course number syntax
if (course < 100){ //Runs if course is less than 100
  coursefixer = "0"; //Sets coursefixer to "0"
  if (course < 10){ //Runs if course is less than 10
    coursefixer = "00"; //Sets course fixer to "00"
  }
}
i = 0; //Sets i back to zero so next loop will run
String department = "department"; //Creates string variable for department
    System.out.print("Department Name: "); //Prompts user for department name
while (i == 0){ //Loop for department, runs when i is equal to 0
  if (myScanner.hasNextDouble()){ //Runs if user enters number, an incorrect input
    junkString = myScanner.next(); //Assigns junkword to get rid of previous user input
    System.out.print("Error, Number Entered, Please Enter Text: "); //Tells user of error and prompts new user input
    i = 0; //Setis i to 0, continuing loop
  }
  else if (myScanner.hasNext()){ //Runs if user enters anything other than a number
    department = myScanner.next(); //Sets department equal to user input
    i = 1; //Set i to 1, ending the loop
  }
}
i = 0; //Sets i back to zero so next loop will run
int timesperweek = 0; //Creates int variable for number of times class meets per week
    System.out.print("Number of Times Class Meets Every Week: "); //Prompts user to input the number of time class meets every week
while (i == 0){ //Loop for timesperweek, runs when i is equal to 0
  if (myScanner.hasNextInt()){ //Runs if user inputs an integer
    timesperweek = myScanner.nextInt(); //Sets timesperweek equal to user input if integer
    i = 1; //Sets i to 1, ending loop if not changed
    if (timesperweek <= 0){ //Runs if int is negative
      i = 0; //Sets i to 0, continuing the loop
      System.out.print("Error, Negative, Please Enter an Positive Integer: "); //Tells user of error and prompts new user input
}
  }
    else { //Runs in previous if previous if statement is false, if user input is not int
      junkString = myScanner.next(); //Assigns junkword to get rid of previous user input
      System.out.print("Error, Not an Integer, Please Enter an Integer: "); //Tells user of error and prompts new user input
    }
}
i = 0; //Sets i back to zero so next loop will run
String ampm = "a.m."; //creates string variable for a.m./p.m. time designation, default is am if not changed
int hour = 0; //creates int variable for hours
int min = 0; //creates int variable for minutes
int start = 0; //creates in variable fort class start time
    System.out.print("Class Start Time (Military Time): "); //Prompts user to input the start class time in military time
while (i == 0){ //Loop for start time, runs when i is equal to 0
  if (myScanner.hasNextInt()){ //Runs if user inputs an integer
    start = myScanner.nextInt(); //Sets start equal to user input if integer
    i = 1; //Sets i to 1, ending loop if not changed
      hour = start/100; //Calculates hours, in 24h, from military time
      min = start-(hour*100); //Calculates minutes from military time
         if (hour >= 24){ //Runs if hours are greater than or equal to 24, an invalid time
           i = 0; //Sets i to 0, continuing the loop
           System.out.print("Invalid Time, Enter Start Time (Military Time): "); //Tells user of error and prompts new user input
         }
         if (min >= 60){ //Runs if minutes are greater than or equal to 60, an invalid time 
           i = 0; //Sets i to 0, continuing the loop
           System.out.print("Invalid Time, Enter Start Time (Military Time): "); //Tells user of error and prompts new user input
         }
        
  }
 else { //Runs in previous if previous if statement is false, if user input is not int
      junkString = myScanner.next(); //Assigns junkword to get rid of previous user input
      System.out.print("Invalid Time, Enter Start Time (Military Time): "); //Tells user of error and prompts new user input
      }
} 
if (start > 1200){ //Runs if user input for time is greater than 1200
      ampm = "p.m."; //Sets ampm equal to p.m.
      start = start-1200; //Calculates hour in 12 hour time
}
if (hour == 0){ //Runs if hour is 0
  hour = 12; //Sets hour equal to 12
}
String minutefixer = ""; //Creates a variable to fix minute syntax
if (min < 10){ //Runs if minute value is less than 10
  minutefixer = "0"; //Inserts zero to create proper minute syntax
}
i = 0; //Sets i back to zero so next loop will run
String instructor = "instructor"; //Creates variable for instructor name
    System.out.print("Instructor Name (Last Name): "); //Prompts user to input the instructor name
while (i == 0){ //Loop for instructure name, runs when i is equal to 0
  if (myScanner.hasNextDouble()){ //Runs if user input is a number
    junkString = myScanner.next(); //Assigns junkword to get rid of previous user input
    System.out.print("Error, Number Entered, Please Enter Text: "); //Tells user of error and prompts new user input
    i = 0; //Sets i to 0, continuing the loop
  }
  else if (myScanner.hasNext()){ //Runs if previous if statement is false, if user input is not a number
    instructor = myScanner.next(); //Sets instructor equal to to user input
    i = 1; //Sets i to 1, ending loop
  }
}
i = 0; //Sets i back to zero so next loop will run
int students = 0; //Creates variable for number of students
    System.out.print("Number of Students: "); //Prompts user to input number of students in class
while (i == 0){ //Loop for number of students, runs when i is equal to 0
  if (myScanner.hasNextInt()){ //Runs if user input is integer
    students = myScanner.nextInt(); //Set students equal to user input
    i = 1; //Sets i to 1, ending loop if not changed
    if (students <= 0){ //Runs if user input is negative
      i = 0; //Sets i to 0, continuing the loop
      System.out.print("Error, Negative, Please Enter an Positive Integer: "); //Tells user of error and prompts new user input
}
  }
    else { //Runs in previous if previous if statement is false, if user input is not int
      junkString = myScanner.next(); //Assigns junkword to get rid of previous user input
      System.out.print("Error, Not an Integer, Please Enter an Integer: "); //Tells user of error and prompts new user input
    }
}
System.out.println(""+department+" "+coursefixer+""+course+", taught by "+instructor+", meets "+timesperweek+" times per week at "+hour+":"+minutefixer+""+min+" "+ampm+", and has "+students+" students."); //Prints all input variables
  }//End of main method
} //End of class